import configparser
from datetime import timedelta

config = configparser.ConfigParser()
config.read("settings.ini")

# Bot
TOKEN = config["Bot"]["Token"]

# Users
ADMINS = [int(admin) for admin in config["Users"]["Admins"].split(",")]
USERS = [int(user) for user in config["Users"]["Users"].split(",")]

# Settings
LANG = config["Settings"]["Language"] if len(config["Settings"]["Language"]) > 0 else "en"
CHRISTMASCOOLDOWN = timedelta(seconds=int(config["Settings"]["ChristmasCooldown"]))
EASTERCOOLDOWN = timedelta(seconds=int(config["Settings"]["EasterCooldown"]))
PROXY = config["Settings"]["Proxy"]

# Resources
CITARCHIVE = config["Resources"]["CitArchive"]
IMGPATH = config["Resources"]["ImgPath"]

# Private
LOGDIR = 'log'
VERSION = '0.5.0-alpha'
