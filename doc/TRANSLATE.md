---
gitea: none
include_toc: true
---

This document explains how to translate the Albatrobot.

To understand it, mind the following:

- `<lang>` is the [2-letters
  code](https://www.gnu.org/software/gettext/manual/gettext.html#Language-Codes)
  of the language you are translating to.
- `<var>` means that you must write the value of `var` instead. For example, if
  you read `foo <year> bar` in 2023 you must write `foo 2023 bar`.
- `.mo` files are binary files that you need to test your translation, but you
  they must *not* be committed.

## Prerequisites

To translate the Albatrobot, you need the [`gettext`
package](https://www.gnu.org/software/gettext/) from the GNU project installed
in your system.

## Translate to a new language

To translate to a new language, create a path named
`locales/<lang>/LC_MESSAGES`, then run `xgettext main.py albatrobot.py -o
locales/<lang>/LC_MESSAGES/template.to`.

Edit `locales/<lang>/LC_MESSAGES/template.to`.  You will find several lines of
metadata and after a serie of groups of rows like the following:

```
#: main.py:59
msgid "help"
msgstr ""
```

The meaning is the following.

* `main.py` is the source file the string comes from.
* `59 ` is the number of the line in the source file where the string begins.
* In `msgid "help"`, `msgid` tells that what follows is the string as it is
  written in the source code, `help` is the string itself. The string is always
  written between quotation marks ( `"..."`).
* In `msgstr ""` tells that what follow is the string translation. It will be
  empty because the string was never translated.

To translate you must edit the content of the quotation marks after `msgstr`,
adding the translation. For example, if you were translating to French the
lines above would become

```
#: main.py:59
msgid "help"
msgstr "aide"
```

Any not translates string will be printed as it is written in the source code.
For example, if you didn't translate to French the example above the Albatrobot
would have printed `help` instead of `aide`.

When you finish the translation, go to `locales/<lang>/LC_MESSAGES` and run
`msgfmt template.po` to produce `messages.mo`.

## Update an existing translation

### Existing strings

If you just want to update the translation of already translated strings, just
edit `locales/<lang>/LC_MESSAGES/template.po`. For an explanation of the
content of that file, see [Translate to a new
language](#translate-to-a-new-language).

When you are happy with your work, run `msgfmt template.to` to produce
`messages.mo`.

### New strings

If you want to translate strings that are present in the source code but not in
`locales/template.pot`, you must update that file. To do so:

1. run `xgettext main.py albatrobot.py -o locales/template.pot`: this will
  update  `locales/template.pot` with the new strings;
2. run ` msgmerge locales/<lang>/LC_MESSAGES/template.po locales/template.pot
  --update`: this will update the translation file of the language you are
  translating to.

You can now go on as desribed in [Existing strings](#existing-strings).

Running `msgmerge` may mark some translations with `#, fuzzy`, such as in

```
#: main.py:59
#, fuzzy
msgid "help"
msgstr "aide"
```

This means that something in these translation changed. You can check them and
if necessary fix them and then remove the `#, fuzzy` line.

### String removal

If a string is removed from the main source code, the next run of `msgmerge` on
each translation will move the translation of such string to the bottom of the
`.po` file and prepend each of its lines with `#~`. You should remove them
before committing the updated translation file.

## Metadata

The first lines of `template.pot` and `template.po` contain metadata. The
parts that must be edited are those in uppercase and their values should be
changed to the following.

| metadata                              | value                                                                                                    |
| ------------------------------------- | -------------------------------------------------------------------------------------------------------- |
| `SOME DESCRIPTIVE TITLE`              | `Albatrabot translation.`                                                                                |
|                                       |                                                                                                          |
| `YEAR THE PACKAGE'S COPYRIGHT HOLDER` | `2021, 2023 bursa-pastoris`                                                                              |
|                                       |                                                                                                          |
| `PACKAGE`                             | `Albatrobot`                                                                                             |
|                                       |                                                                                                          |
| `FIRST AUTHOR <EMAIL@ADDRESS>, YEAR`  | `bursa-pastoris <bursapastoris at disroot dot org>, 2021, 2023.`                                         |
|                                       |                                                                                                          |
| `PACKAGE VERSION`                     | The value of the `VERSION` variable in [`constants.py`](../constants.py), *without* the `\`s             |
|                                       |                                                                                                          |
| `YEAR-MO-DA HO:MI+ZONE`               | `<year>-<month>-<day> <hour>:<minute>+<timezone>` the translation was created at and in.[^creation-time] |
|                                       |                                                                                                          |
| `FULL NAME <EMAIL@ADDRESS>`           | `<translator's name or nickname> <translator's email address>`                                           |
|                                       |                                                                                                          |
| `LANGUAGE <LL@li.org>`                | `<language of the translation> <translation team's email address>`                                       |
|                                       |                                                                                                          |
| `CHARSET`                             | `UTF-8`                                                                                                  |


[^creation-time]: You can use any value from day on, as long as the resulting
  moment comes after the previous udpate and before you commit the translation.
