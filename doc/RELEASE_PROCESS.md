To release a new version:

1. Checkout a new branch named `release-v{release}`
2. Complete the checklist *in given order*.  If anything changes, restart from
   the first step.

   - [ ] Check if dependencies are up to date
   - [ ] Reorder stuff in source code (imports, requirements, function
         definitions, command handlers...)
   - [ ] Update docstrings and comments
   - [ ] Update /help messages
   - [ ] Update translation
     - [ ] the template
     - [ ] each known language
   - [ ] Update version number
     - [ ] in `constants.py`
     - [ ] in the the metadata of updated translations
   - [ ] Fix and update documentation not listed below
   - [ ] Fix and update `doc/LEGAL.pdf`
   - [ ] Fix and update `README.md`
   - [ ] Fix and update `doc/CHANGELOG.md`
   - [ ] _At least now_, test _each command_, both as admin and simple user, to
         identify new bugs; testing English is mandatory, all other languages
         are recommended
   - [ ] Update copyright notices years
     - [ ] In file headers
     - [ ] In copyright_footer() definition and its translations
   - [ ] If the items were checked, *uncheck them*!

   Using at least one commit per item is recommended. They can be squashed
   afterwards.

3. Merge the branch in `master`
4. Tag the merge commit as `v{release}` and push
