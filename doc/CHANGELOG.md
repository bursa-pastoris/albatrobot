---
gitea: none
include_toc: true
---

## v0.5.0-alpha
### Features

- Remove `/epiphony` command due to maintenance issues
- Add `/carnival`, to berate users who use a certain word
- Support anonymous cits
- Provide licensing info to users not authorized to use the bot

### Misc

- Convert CSV files to JSON
- Improve randomness (by substituting random module with secrets)
- Shutdown gracefully
- Improve the release process
- Update dependencies
- Misc fixes


## v0.4.0
### Misc

- Fix `VERSION` in `constants.py` to allow Telegram markup
- Fix Italian translation

## v0.4.0-alpha
### Features

- In groups, `/Epiphony` now:
  - deletes the message issuing the command,
  - sends the voice message as an answer to the same message to wich the
    command was issued as a reply to (if any)
- Add support for captions to `/Christmas` images
- `/Christmas` and `/Easter` cooldown times are now independent from each other

### Misc

- If no language is set, default to English
- `/legal` now sends the information in PDF (instead of TXT)
- If the instance is in a language other than English, `/legal` now explains
  that and why the legal info will not be translated
- Fix privacy info (without privacy implications: simply, `/legal` was added to
  the list of commands that generate anonymous logs)
- Fix Italian translation
- Fix and improve documentation
- Code cleanup
- Minor fixes and enhancements


## v0.3.1

### Misc

- Update copyright headers
- Add inline suggestions for all common commands that don't require arguments
- Add stuff to v0.3 changelog

## v0.3
### New features

- Inline suggestions
- New commands:
  - `/epiphony`, to let Albatrobot speak
  - `/init`, to set again commands suggestions and clean `/epiphony` "cache"
- Support for proxy
- Support for roll modifiers

### Enhancements

- Better help texts
- Citations for /easter are now stored in a CSV file
- Enhance "no images available" image

### Bug fixes

- Close #4, #5 and #6

### Code

- Constants moved to own module

### Misc

- Update dependencies, including `python-telegram-bot` to v20
- Misc cleanup and fixes


## v0.2

- Added admins allowed to stop the bot from Telegram
- Completely anonymized logs
- Added privacy info
- Improved logs
- License compliance and update
- General cleanup
- Documentation fixes
