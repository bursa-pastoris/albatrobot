I develop ALbatrobot for my own use.

Everyone is welcome to propose his contribution:

1. through pull requests on the [Disroot
   repository](https://git.disroot.org/bursa-pastoris/albatrobot/pulls), or
2. by email at `bursapastoris at disroot dot org`, as either patches or pull
   requests from other repositories.[^email]

But note that, as Albatrobot is a personal project, I will merge only the
contributions that I find useful or interesting *for me*.  However, everyone
can enjoy the rights granted by [AGPL-3.0-only license](./LICENSE) to fork the
project and do whatever he want on their copies (as long as the license is
honoured).


[^email]: If you want to send contributes by email, see
  [here](https://git.disroot.org/bursapastoris/info/contact.md) for more
  information about how to contact me by email and
  [here](https://git-scm.com/book/en/v2/Distributed-Git-Contributing-to-a-Project)
  for how to use `git format-patch`.
