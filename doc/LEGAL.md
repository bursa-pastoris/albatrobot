## Privacy

Albatrobot's developer does not process any personal data under the definition
of the
[GDPR](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32016R0679)
and therefore is not a data controller.

Albatrobot automatically processes users personal data in the following
situations only.

1. _When command `/end_of_year_dinner` (or its translated version) is used._
   The user who uses it and the recipient of the messages will see each other's
   Telegram name. This data is not stored by Albatrobot and no other data is
   ever disclosed to other users.
2. _When command `/carnival <word>` (or its translated version) is used_ in a
   chat. The user's Telegram ID is stored temporarily (and never written to
   disk) to make sure that only one bad word is enabled for each user in the
   chat. Such ID is removed as soon as someone uses the bad word, or when the
   same user removes it using the `/carnival` command  in the same chat and
   without arguments.

Note however that:

1. to use the Albatrobot a Telegram account is needed and therefore [Telegram's
   privacy policy applies](https://telegram.org/privacy);
2. the operator of the instance must add each user's Telegram ID to
   `settings.ini` to authorize them to use the bot.

Also, anonymous logs are stored:

* when a user uses one of the following commands:
  * `/christmas`, if the user hits the time limit
  * `/easter`, if the user hits the time limit or asks for an unknown author
  * `/end_of_year_dinner`
  * `/legal`
  * `/source`
* whenever an error is raised

The logs are intended for debug and issue resolution only and *never* contain
personal data.

## Copyright

Albatrobot is proudly [free
software](https://www.gnu.org/philosophy/free-sw.html#four-freedoms) (as both
in "free speech" and "free beer"), and always will be, as it is released under
[AGPL-3.0-only](https://www.gnu.org/licenses/agpl-3.0-standalone.html) license.
Its source code is publicly available at
[Disroot](https://git.disroot.org/bursapastoris/albatrobot).

**Disclaimer:** Albatrobot is free software, but it works interacting with
[Telegram](https://telegram.org)'s servers and API. Most of Telegram's clients
are distributed under a free license, but *all Telegram's servers' code is
proprietary software* and as such it yields unjust power over its users and
cannot be trusted. *Albatrobot's development does not want to promote non-free
software* and is just an instructive hobby.

Please, *at least* for communication that must be kept private or secret do not
use Telegram. Use only free software and free protocols instead.
