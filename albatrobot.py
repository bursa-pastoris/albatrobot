# Albatrobot - Telegram bot for RPG groups and similar
# Copyright (C) 2019, 2020, 2021, 2022, 2023, 2024, 2025 bursa-pastoris
#
# This file is part of Albatrobot.
#
# Albatrobot is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# Albatrobot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Albatrobot.  If not, see <https://www.gnu.org/licenses/>.


from datetime import datetime
import json
import logging
import os
from re import findall as find
import secrets
import time

import Levenshtein
from rolldice import roll_dice, rolldice
from telegram import BotCommand
from telegram.constants import ChatAction, ChatMemberStatus, ChatType, ParseMode
from telegram.ext import Application

from constants import *

# Prepare paths
required_paths = [LOGDIR]
for i in required_paths:
    os.makedirs(i, exist_ok=True)

# Logging
logger = logging.getLogger('albatrobot_logger')
logger.setLevel(logging.DEBUG)
logformat = logging.Formatter('%(asctime)s %(levelname)s: %(message)s')

loghandler_toconsole = logging.StreamHandler()
loghandler_toconsole.setLevel(logging.DEBUG)
loghandler_toconsole.setFormatter(logformat)

loghandler_tofile = logging.FileHandler(f'{LOGDIR}/albatrobot.log')
loghandler_tofile.setLevel(logging.DEBUG)
loghandler_tofile.setFormatter(logformat)

logger.addHandler(loghandler_toconsole)
logger.addHandler(loghandler_tofile)


# Internal functions
def closest(good_list, item):
    """Return the item(s) of good_list closest to item.

    "Closest" means "with the shortest Levenshtein distance".
    None values in good_list are ignored.
    """
    distances = {}
    for i in good_list:
        if i != None:
            i_dist = Levenshtein.distance(i, item)
            if i_dist not in distances:
                distances[i_dist] = list()
            distances[i_dist].append(i)
    closest_items = distances[min(distances.keys())]
    return closest_items


def copyright_footer(msg):
    """Append a footer with copyright information to msg.

    msg must be escaped for telegram.constants.ParseMode.MARKDOWN_V2 and the
    output is as well.
    """
    separator = '\n\\-\\-\n'
    footer_text = _(
        'Albatrobot \\- Telegram bot for RPG groups and similar\n'
        'Copyright \\(C\\) 2019, 2020, 2021, 2022, 2023, 2024, 2025 bursa\\-pastoris\n'
        '\n'
        'Albatrobot is free software: its source code is distributed under'
        ' [AGPL\\-3\\.0\\-only license](https:\\/\\/www.gnu.org\\/licenses\\/agpl-3.0.txt)'
        ' and is publicly available on'
        ' [Disroot](https:\\/\\/git.disroot.org\\/bursapastoris\\/albatrobot)\\.'
        )
    with_footer = msg + '\n' + separator + '_' + footer_text + '_'
    return with_footer


async def berate(context):
    """Berate a user for using a word."""
    reply_to = context.job.data[0]
    trap = context.job.data[1]
    await context.bot.send_message(
        chat_id=context.job.chat_id,
        reply_to_message_id=reply_to,
        text = _("AAAH! Don't say \"{trap}\"!".format(trap=trap))
    )


# Basic functions
async def start(update, context):
    """Show basic info when a user contacts the bot for the first time."""
    greeting = _("Hello, I'm Albatrobot\\! Type \\/help for help\\!")

    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=copyright_footer(greeting),
        disable_web_page_preview=True,
        parse_mode=ParseMode.MARKDOWN_V2
    )


async def stop(update, context):
    """Stop the bot."""
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=_('Albatrobot will be stopped in a few moments.')
    )
    logger.info('Albatrobot was stopped.')
    Application.stop_running(context.application)


async def help(update, context):
    """Show a help message.

    The message doesn't correspond to the inline suggestions set in
    @BotFather. They can be set issuing the command /init.
    """
    # Common commands
    com_header = _('*Albatrobot commands*')
    com_help = {
        _('carnival') + _(' `\\(\\<word\\>\\)`'):
            _('set a bad word for this chat\\. If someone uses it, he will be'
              ' berated in a few seconds\\! You can set only one bad word per'
              ' chat, existing one will be overwritten by the new one\\.'
              ' Launch without arguments to remove your bad word from current'
              ' chat\\.'),
        _('christmas'):
            _('get an image'),
        _('easter') + _(' `\\(\\<author\\>\\)`'):
            _('get a citation from given author, or a random one if no author'
              'is specified\\.')
            + _('Use `/easter {}` to get an anonymous citation\\.').format(_('anonymous')),
        _('end\\_of\\_year\\_dinner'):
            _('send all citations in the archive to a random user\\.'),
        _('help'):
            _('get help about bot commands\\.'),
        _('legal'):
            _('get legal info about privacy and copyright\\.'),
        _('roll') + _(' `\\<dice\\>\\(\\<modifiers\\>\\)`'):
            _('roll dice, with or without modifiers\\. Launch without'
              ' arguments to get help on the modifiers\\.'),
        _('version'):
            _("get Albatrobot's version")
    }
    com_help = [f'\\- \\/{i}: {com_help[i]}' for i in com_help]
    com_help.sort()
    com_help = '\n'.join(com_help)
    com_help = f'{com_header}\n{com_help}'

    # Admin commands
    adm_header = _('*Additional commands for bot admins*')
    adm_help = {
        _('reset'): _('reset the bot\\. This updates inline suggestions and'
            ' removes scheduled jobs\\.'),
        _('stop'): _('stop Albatrobot\\. To be used _only_ in case of'
            ' emergency: restarting the bot after this command is used'
            ' requires direct access to the server\\.')
    }
    adm_help = [f'\\- \\/{i}: {adm_help[i]}' for i in adm_help]
    adm_help.sort()
    adm_help = '\n'.join(adm_help)
    adm_help = f'{adm_header}\n{adm_help}'

    # Addendum
    nonadm_addendum = _(
        'Bot admins have access to additional commands and can get help on'
        ' them using /help in a private chat\\.'
    )

    # Actually send stuff
    if (update.effective_chat.type == ChatType.PRIVATE
            and update.effective_user.id in ADMINS):
        help_text = _(f'{com_help}\n\n{adm_help}')
    else:
        help_text = _(f'{com_help}\n\n{nonadm_addendum}')

    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=help_text,
        parse_mode=ParseMode.MARKDOWN_V2
    )


async def unauthorized_user(update, context):
    """Inform the user that he is not authorized to used the bot."""
    error = _("Sorry, you are not authorized to use this bot\\.")
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=copyright_footer(error),
        disable_web_page_preview=True,
        parse_mode=ParseMode.MARKDOWN_V2,
    )


async def unknown_command(update, context):
    """Inform the user that the command does not exist."""
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=_('Unknown command.')
    )


async def legal(update, context):
    """
    Send legal information about privacy and copyright.
    """
    logger.info('A user asked for legal info.')
    if LANG != "en":
        note=_('The only official version is the English one and any'
               ' imperfect translation would be misleading. Therefore, this'
               ' document will not be translated.')
    else:
        note=""
    await context.bot.send_chat_action(
        chat_id=update.effective_chat.id,
        action=ChatAction.UPLOAD_DOCUMENT)
    with open('doc/LEGAL.pdf','rb') as legalnotes:
        await context.bot.send_document(
            chat_id=update.effective_chat.id,
            document=legalnotes,
            filename='albatrobot-legal-notes.pdf',
            caption=note
            )


async def reset(update, context):
    """Reset the bot.

    This means resetting inline command suggestions and removing scheduled
    jobs.
    """
    # Set commands suggestions
    commands = [BotCommand(_('carnival'), _('Set a bad word')),
                BotCommand(_('christmas'), _('Get an image')),
                BotCommand(_('easter'), _('Get a citation')),
                BotCommand(_('end_of_year_dinner'),
                           _('Send all citations to a random user')),
                BotCommand(_('roll'), _('Roll dice')),
                BotCommand(_('help'), _('Get help'))]

    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=_('Initializing...'))

    await context.bot.set_my_commands(commands)

    # Remove scheduled jobs (currently, carnival only)
    for i in context.job_queue.jobs():
        i.schedule_removal()

    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=_('Done'))


async def version(update, context):
    """Send Albatrobot's current version."""
    escaped_version = VERSION.replace('.', '\\.').replace('-', '\\-')
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=_("*Albatrobot's version:* {}").format(escaped_version),
        parse_mode=ParseMode.MARKDOWN_V2
    )


# Commands
async def carnival_set(update, context):
    """Set a trap on a string to yell on the first user who uses it."""
    match len(context.args):
        case 0:
            user_id = update.effective_user.id
            if ('carnival' in context.chat_data.keys()
                    and user_id in context.chat_data['carnival'].keys()):
                del context.chat_data['carnival'][user_id]
            await context.bot.send_message(
                chat_id=update.effective_chat.id,
                reply_to_message_id=update.effective_message.message_id,
                text = _("Carnival parade cancelled")
            )
        case 1:
            trap = context.args[0].casefold()
            user_id = update.effective_user.id

            if 'carnival' not in context.chat_data.keys():
                context.chat_data['carnival'] = dict()

            context.chat_data['carnival'][user_id] = trap

            # If in group chat and can delete messages, delete the message
            bot_data = await context.bot.get_me()

            bot_chatmember = await context.bot.get_chat_member(
                chat_id=update.effective_chat.id,
                user_id=bot_data.id)

            if (bot_chatmember.status == ChatMemberStatus.ADMINISTRATOR
                    and bot_chatmember.can_delete_messages):
                    # The first condition:
                    # 1. checks that the chat is a group;
                    # 2. prevents AttributeError if the bot is only
                    #    ChatMemberStatus.MEMBER
                await context.bot.delete_message(
                    chat_id=update.effective_chat.id,
                    message_id=update.effective_message.message_id)

            # Anyway, acknowledge the trap was set
            await context.bot.send_message(
                chat_id=update.effective_chat.id,
                text = _("New carnival parade accepted!")
            )
        case _:
            await context.bot.send_message(
                chat_id=update.effective_chat.id,
                reply_to_message_id=update.effective_message.message_id,
                text=_("You need exactly one word for carnival!")
            )


async def carnival_handle(update, context):
    """Activate and consume carnival traps."""
    chat_id = update.effective_chat.id
    message_id = update.effective_message.message_id

    traps = context.chat_data.get('carnival', dict())
    consumed_traps = list()
    for i in traps:
        t = traps[i]
        if t in update.effective_message.text.casefold():
            context.job_queue.run_once(
                    berate,
                    # Randomize delay
                    secrets.choice(range(5,31)),
                    data=(message_id, t),
                    chat_id=chat_id)
            consumed_traps.append(i)
    for i in consumed_traps:
        del context.chat_data['carnival'][i]


async def christmas(update, context):
    """Send a random image from the archive."""
    last_msg = context.user_data.get('last_christmas',
        datetime.fromisoformat('1970-01-01T00:00'))
    if datetime.now()-last_msg < CHRISTMASCOOLDOWN:
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=_("Christmas is good, but you can't invoke it so often! Try"
                   " again in some seconds...")
        )
        logger.info('A user wants too much Christmas.')
        return

    with open(f'{IMGPATH}/images.json','r') as images_file:
        imgs = json.load(images_file)

    img = secrets.choice(list(imgs.keys()))
    img_file = img
    img_capt = imgs[img]

    await context.bot.send_chat_action(
       chat_id=update.effective_chat.id,
       action=ChatAction.UPLOAD_PHOTO
    )
    await context.bot.send_photo(
        chat_id=update.effective_chat.id,
        photo=open(f'{IMGPATH}/{img_file}', 'rb'),
        caption=img_capt
    )
    context.user_data['last_christmas'] = datetime.now()


async def easter(update, context):
    """Send a citation from the archive.

    User can either choose the author or let the bot pick a random one.
    """
    last_msg = context.user_data.get('last_easter',
        datetime.fromisoformat('1970-01-01T00:00'))

    if datetime.now()-last_msg < EASTERCOOLDOWN:
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=_("Easter is good, but you can't invoke it so often!"
             " Retry in some seconds...")
        )
        logger.info('A user wants too much Easter.')
        return

    with open(CITARCHIVE,'r') as cits_file:
        cits = json.load(cits_file)

    authors = tuple(cits[i]['author'] for i in cits)

    # If the user asked for an author...
    if len(context.args) > 0:
        # ...and it exists
        if context.args[0] in authors:
            author = str(context.args[0])
        elif context.args[0] == _('anonymous'):
            author = None
        # ...and it does not exist
        else:
            closest_authors = closest(authors, context.args[0])
            if len(closest_authors) == 1:
                suggestion = _('The most similar is {}.').format(closest_authors[0])
            else:
                last_author = closest_authors[-1]
                other_authors = ', '.join(closest_authors[:-1])
                suggestion = _('The most similar are {} and {}.').format(
                    other_authors, last_author)
            await context.bot.send_message(
                chat_id=update.effective_chat.id,
                text=_('Author {required_author} is not present in the'
                       ' archive! {suggestion}\n'
                    '\n'
                    'N.B.: case is important!').format(
                         required_author=context.args[0],
                         suggestion=suggestion)
            )
            logger.info('A user wants easter from non registered'
                ' author {author}.'.format(author=context.args[0]))
            return
    # If the user did not ask for an author
    else:
        author = secrets.choice(authors)

    with open(CITARCHIVE, 'r') as cits_file:
        cits = json.load(cits_file)
        cits_pool = [cits[i]['text'] for i in cits
                     if cits[i]['author'] == author]

    # If the citation is anonymous, set the printed author to "anonymous"
    if author == None:
        author = _('anonymous')

    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=_('{cit}\n\n~{author}').format(
            cit=secrets.choice(cits_pool),
            author=author
        )
    )
    context.user_data['last_easter'] = datetime.now()


async def end_of_year_dinner(update, context):
    """Send a random user each citation from the archive."""
    with open(CITARCHIVE,'r') as cits_file:
        cits_archive = json.load(cits_file)

    cits_archive = [cits_archive[i] for i in cits_archive]
    cits_number = len(cits_archive)
    dest_user_id = secrets.choice(USERS)
    dest_user = await context.bot.getChatMember(chat_id=dest_user_id,
                                                user_id=dest_user_id)
    dest_user_name = dest_user.user.first_name
    done_notification = _('End of year dinner of {num} plates delivered to'
                          ' {dest}').format(num=cits_number, dest=dest_user_name)

    await context.bot.send_message(
        chat_id=dest_user_id,
        text=_('Hello there, here an end of year dinner offered by {}!').format(
            update.message.from_user.first_name)
    )

    for cit in cits_archive:
        await context.bot.send_message(
            chat_id=dest_user_id,
            text=_('{cit}\n\n~{author}'.format(cit=cit['text'],
                                               author=cit['author']))
        )
        time.sleep(1.1)     # To avoid hitting Telegram's message rate limit

    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=done_notification
    )
    logger.info('A user sent an end of year dinner.')


async def roll(update, context):
    """Roll a dice following DnD notation: <dice number>d<die faces>.

    Many modifiers are supported. Dice number and faces are limited to 500
    each, to prevent DoS effects.
    """
    to_roll = ''.join(context.args)
    if len(to_roll) == 0:
        roll_modifiers = {
            '\\+n': _('add `n` to the roll'),
            '\\-n': _('subtract `n` from the roll'),
            '\\*n': _('multiply the roll by `n`'),
            '/n': _('divide the roll by `n`'),
            '//n': _('floor divide the roll by `n`'),
            '\\*\\*n': _('exponentiate the roll to `n`'),
            'K\\(n\\)': _('keep highest `n` rolls \\(defaults to `1`\\)'),
            'k\\(n\\)': _('keep lowest `n` rolls \\(defaults to `1`\\)'),
            'X\\(n\\)': _('drop highest `n` rolls \\(defaults to `1`\\)'),
            'x\\(n\\)': _('drop lowest `n` rolls \\(defaults to `1`\\)'),
            '\\!\\(n\\)': _('explode rolls equal to `n` \\(defaults to die size\\)'),
            '\\!\\<n': _('explode rolls lower than `n`'),
            '\\!\\>n': _('explode rolls higher than `n`'),
            'f\\<n': _('count failures as rolls lower than `n`'),
            'f\\>n': _('count failures as rolls higher than `n`'),
            '\\<n': _('count successes as rolls lower than `n`'),
            '\\>n': _('count successes as rolls higher than `n`'),
            '\\!p': _('penetrate rolls equal to dice size'),
            '\\!p\\<n': _('penetrate rolls lower than `n`'),
            '\\!p\\>n': _('penetrate rolls higher than `n`'),
            'an': _('add `n` to each roll'),
            'sn': _('subtract `n` from each roll'),
            'mn': _('multiply by `n` each roll'),
            'R\\(n\\)': _('reroll each dice that rolled `n` until there are no'
                          ' `n` left\\(defaults to `1`\\)'),
            'r\\(n\\)': _('reroll once each dice that rolled `n` \\(defaults to'
                          ' 1\\)'),
            'R\\<n': _('reroll dice that rolled lower than `n` until there are'
                       ' no results lower than `n` left'),
            'R\\>n': _('reroll dice that rolled higher than `n` until there'
			           ' no results higher than `n` left'),
            'r\\<n': _('reroll once dice that roll lower than `n`'),
            'r\\>n': _('reroll once dice that roll higher than `n`')
            }
        roll_modifiers = [f'\\- `{i}`: {roll_modifiers[i]}' for i in roll_modifiers]
        roll_modifiers = '\n'.join(roll_modifiers)
        roll_help = _(
            "To roll, use the syntax `\\<number of dice\\>d\\<dice size\\>`\\."
            " You can also use the following modifiers\\. Note that:\\n"
            "\\- `\\(n\\)` means that an integer number may be specified, but"
            " if it isn't the specified default value is assumed\n"
            "\\- `n` means that an integer number is required\n"
            "\\- any other symbol means exactly that symbol\n"
            "\n"
            "\n"
        ) + roll_modifiers    # "+" is a workaround to incompatibility between
                              # f-strings and parse_mode
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=roll_help,
            disable_web_page_preview=True,
            parse_mode=ParseMode.MARKDOWN_V2
        )
    elif max([int(i) for i in find(r'\d+',to_roll)]) > 500:    # Prevent DoS
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=_('You can roll up to 500 dice with up to 500 faces at once!')
        )
    else:
        try:
            the_roll = roll_dice(to_roll)
            await context.bot.send_message(
                chat_id=update.effective_chat.id,
                text=_('{user_name} rolled {rolls} = {result}').format(
                    user_name=update.message.from_user.first_name,
                    rolls=the_roll[1],
                    result=the_roll[0]
                )
            )
        except rolldice.DiceGroupException:
            await context.bot.send_message(
                chat_id=update.effective_chat.id,
                text=_('You used a wrong syntax!')
            )
