#!/bin/python3

# Albatrobot - Telegram bot for RPG groups and similar
# Copyright (C) 2019, 2020, 2021, 2022, 2023, 2024, 2025 bursa-pastoris
#
# This file is part of Albatrobot.
#
# Albatrobot is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# Albatrobot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Albatrobot.  If not, see <https://www.gnu.org/licenses/>.

import gettext
from telegram import Update
from telegram.ext import ApplicationBuilder, ContextTypes, CommandHandler, MessageHandler, filters

import albatrobot
from constants import *

# Localization
transl = gettext.translation('messages', localedir='locales',languages=[LANG])
transl.install()
_ = transl.gettext


# Link to bot platform
application = ApplicationBuilder().token(TOKEN)
if PROXY != '':
    application = application.proxy(PROXY).get_updates_proxy(PROXY)
application = application.build()


# Filter unauthorized users
application.add_handler(
    MessageHandler((~filters.User(USERS)),
                   albatrobot.unauthorized_user)
    )
application.add_handler(
    CommandHandler(['stop', _('stop'), 'reset', _('reset')],
                   albatrobot.unauthorized_user,
                   (~filters.User(ADMINS)))
    )


# Basic functions
application.add_handler(
    CommandHandler('start',
                   albatrobot.start)
    )
application.add_handler(
    CommandHandler(['stop', _('stop')],
                   albatrobot.stop,
                   filters.User(ADMINS))
    )
application.add_handler(
    CommandHandler(['help', _('help')],
                   albatrobot.help)
    )
application.add_handler(
    CommandHandler(['reset', _('reset')],
                   albatrobot.reset,
                   filters.User(ADMINS))
    )
application.add_handler(
    CommandHandler(['legal', _('legal')],
                   albatrobot.legal)
    )
application.add_handler(
    CommandHandler(['version', _('version')],
                   albatrobot.version)
    )


# Commands
application.add_handler(
    CommandHandler(['carnival', _('carnival')],
                   albatrobot.carnival_set)
    )
application.add_handler(
    MessageHandler(filters.TEXT & ~filters.COMMAND,
                   albatrobot.carnival_handle)
    )
application.add_handler(
    CommandHandler(['christmas', _('christmas')],
                   albatrobot.christmas)
    )
application.add_handler(
    CommandHandler(['easter', _('easter')],
                   albatrobot.easter)
    )
application.add_handler(
    CommandHandler(['end_of_year_dinner', _('end_of_year_dinner')],
                   albatrobot.end_of_year_dinner)
    )
application.add_handler(
    CommandHandler(['roll', _('roll')],
                   albatrobot.roll)
    )

# Unknown commands
application.add_handler(
    MessageHandler(filters.COMMAND,
                   albatrobot.unknown_command)
    )

# Start bot
application.run_polling()
