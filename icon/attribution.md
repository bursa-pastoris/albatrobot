The icone is a derivative work of
* Jifutari,
  [Short tailed Albatross1.jpg](https://commons.wikimedia.org/wiki/File:Short_tailed_Albatross1.jpg)
  (GFDL 1.2+)
* Anbox team, redraw by SweetCanadianMullet,
  [Anbox logo.svg](https://commons.wikimedia.org/wiki/File:Anbox_logo.svg)
  (GPL 3+)
