# Translation template for Albatrobot.
# Copyright (C) 2021, 2023, 2025 bursa-pastoris
# This file is distributed under the same license as the Albatrobot package.
# bursa-pastoris <bursapastoris@disroot.org>, 2021, 2023, 2025
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Albatrobot v0.5.0-alpha\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2025-01-04 00:00+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: main.py:46 main.py:58 albatrobot.py:172
msgid "stop"
msgstr ""

#: main.py:46 main.py:67 albatrobot.py:170
msgid "reset"
msgstr ""

#: main.py:63 albatrobot.py:152 albatrobot.py:256
msgid "help"
msgstr ""

#: main.py:72 albatrobot.py:154
msgid "legal"
msgstr ""

#: main.py:76 albatrobot.py:159
msgid "version"
msgstr ""

#: main.py:83 albatrobot.py:138 albatrobot.py:250
msgid "carnival"
msgstr ""

#: main.py:91 albatrobot.py:144 albatrobot.py:251
msgid "christmas"
msgstr ""

#: main.py:95 albatrobot.py:146 albatrobot.py:252
msgid "easter"
msgstr ""

#: main.py:99 albatrobot.py:253
msgid "end_of_year_dinner"
msgstr ""

#: main.py:103 albatrobot.py:156 albatrobot.py:255
msgid "roll"
msgstr ""

#: albatrobot.py:83
msgid ""
"Albatrobot \\- Telegram bot for RPG groups and similar\n"
"Copyright \\(C\\) 2019, 2020, 2021, 2022, 2023, 2024, 2025 bursa\\-pastoris\n"
"\n"
"Albatrobot is free software: its source code is distributed under "
"[AGPL\\-3\\.0\\-only license](https:\\/\\/www.gnu.org\\/licenses\\/"
"agpl-3.0.txt) and is publicly available on [Disroot](https:\\/\\/"
"git.disroot.org\\/bursapastoris\\/albatrobot)\\."
msgstr ""

#: albatrobot.py:102
#, python-brace-format
msgid "AAAH! Don't say \"{trap}\"!"
msgstr ""

#: albatrobot.py:109
msgid "Hello, I'm Albatrobot\\! Type \\/help for help\\!"
msgstr ""

#: albatrobot.py:123
msgid "Albatrobot will be stopped in a few moments."
msgstr ""

#: albatrobot.py:136
msgid "*Albatrobot commands*"
msgstr ""

#: albatrobot.py:138
msgid " `\\(\\<word\\>\\)`"
msgstr ""

#: albatrobot.py:139
msgid ""
"set a bad word for this chat\\. If someone uses it, he will be berated in a "
"few seconds\\! You can set only one bad word per chat, existing one will be "
"overwritten by the new one\\. Launch without arguments to remove your bad "
"word from current chat\\."
msgstr ""

#: albatrobot.py:145
msgid "get an image"
msgstr ""

#: albatrobot.py:146
msgid " `\\(\\<author\\>\\)`"
msgstr ""

#: albatrobot.py:147
msgid ""
"get a citation from given author, or a random one if no authoris specified\\."
msgstr ""

#: albatrobot.py:149
msgid "Use `/easter {}` to get an anonymous citation\\."
msgstr ""

#: albatrobot.py:149 albatrobot.py:416 albatrobot.py:451
msgid "anonymous"
msgstr ""

#: albatrobot.py:150
msgid "end\\_of\\_year\\_dinner"
msgstr ""

#: albatrobot.py:151
msgid "send all citations in the archive to a random user\\."
msgstr ""

#: albatrobot.py:153
msgid "get help about bot commands\\."
msgstr ""

#: albatrobot.py:155
msgid "get legal info about privacy and copyright\\."
msgstr ""

#: albatrobot.py:156
msgid " `\\<dice\\>\\(\\<modifiers\\>\\)`"
msgstr ""

#: albatrobot.py:157
msgid ""
"roll dice, with or without modifiers\\. Launch without arguments to get help "
"on the modifiers\\."
msgstr ""

#: albatrobot.py:160
msgid "get Albatrobot's version"
msgstr ""

#: albatrobot.py:168
msgid "*Additional commands for bot admins*"
msgstr ""

#: albatrobot.py:170
msgid ""
"reset the bot\\. This updates inline suggestions and removes scheduled "
"jobs\\."
msgstr ""

#: albatrobot.py:172
msgid ""
"stop Albatrobot\\. To be used _only_ in case of emergency: restarting the "
"bot after this command is used requires direct access to the server\\."
msgstr ""

#: albatrobot.py:183
msgid ""
"Bot admins have access to additional commands and can get help on them "
"using /help in a private chat\\."
msgstr ""

#: albatrobot.py:190
#, python-brace-format
msgid ""
"{com_help}\n"
"\n"
"{adm_help}"
msgstr ""

#: albatrobot.py:192
#, python-brace-format
msgid ""
"{com_help}\n"
"\n"
"{nonadm_addendum}"
msgstr ""

#: albatrobot.py:203
msgid "Sorry, you are not authorized to use this bot\\."
msgstr ""

#: albatrobot.py:216
msgid "Unknown command."
msgstr ""

#: albatrobot.py:226
msgid ""
"The only official version is the English one and any imperfect translation "
"would be misleading. Therefore, this document will not be translated."
msgstr ""

#: albatrobot.py:250
msgid "Set a bad word"
msgstr ""

#: albatrobot.py:251
msgid "Get an image"
msgstr ""

#: albatrobot.py:252
msgid "Get a citation"
msgstr ""

#: albatrobot.py:254
msgid "Send all citations to a random user"
msgstr ""

#: albatrobot.py:255
msgid "Roll dice"
msgstr ""

#: albatrobot.py:256
msgid "Get help"
msgstr ""

#: albatrobot.py:260
msgid "Initializing..."
msgstr ""

#: albatrobot.py:270
msgid "Done"
msgstr ""

#: albatrobot.py:278
msgid "*Albatrobot's version:* {}"
msgstr ""

#: albatrobot.py:295
msgid "Carnival parade cancelled"
msgstr ""

#: albatrobot.py:326
msgid "New carnival parade accepted!"
msgstr ""

#: albatrobot.py:332
msgid "You need exactly one word for carnival!"
msgstr ""

#: albatrobot.py:364
msgid ""
"Christmas is good, but you can't invoke it so often! Try again in some "
"seconds..."
msgstr ""

#: albatrobot.py:400
msgid ""
"Easter is good, but you can't invoke it so often! Retry in some seconds..."
msgstr ""

#: albatrobot.py:422
msgid "The most similar is {}."
msgstr ""

#: albatrobot.py:426
msgid "The most similar are {} and {}."
msgstr ""

#: albatrobot.py:430
#, python-brace-format
msgid ""
"Author {required_author} is not present in the archive! {suggestion}\n"
"\n"
"N.B.: case is important!"
msgstr ""

#: albatrobot.py:455 albatrobot.py:486
#, python-brace-format
msgid ""
"{cit}\n"
"\n"
"~{author}"
msgstr ""

#: albatrobot.py:474
#, python-brace-format
msgid "End of year dinner of {num} plates delivered to {dest}"
msgstr ""

#: albatrobot.py:479
msgid "Hello there, here an end of year dinner offered by {}!"
msgstr ""

#: albatrobot.py:507
msgid "add `n` to the roll"
msgstr ""

#: albatrobot.py:508
msgid "subtract `n` from the roll"
msgstr ""

#: albatrobot.py:509
msgid "multiply the roll by `n`"
msgstr ""

#: albatrobot.py:510
msgid "divide the roll by `n`"
msgstr ""

#: albatrobot.py:511
msgid "floor divide the roll by `n`"
msgstr ""

#: albatrobot.py:512
msgid "exponentiate the roll to `n`"
msgstr ""

#: albatrobot.py:513
msgid "keep highest `n` rolls \\(defaults to `1`\\)"
msgstr ""

#: albatrobot.py:514
msgid "keep lowest `n` rolls \\(defaults to `1`\\)"
msgstr ""

#: albatrobot.py:515
msgid "drop highest `n` rolls \\(defaults to `1`\\)"
msgstr ""

#: albatrobot.py:516
msgid "drop lowest `n` rolls \\(defaults to `1`\\)"
msgstr ""

#: albatrobot.py:517
msgid "explode rolls equal to `n` \\(defaults to die size\\)"
msgstr ""

#: albatrobot.py:518
msgid "explode rolls lower than `n`"
msgstr ""

#: albatrobot.py:519
msgid "explode rolls higher than `n`"
msgstr ""

#: albatrobot.py:520
msgid "count failures as rolls lower than `n`"
msgstr ""

#: albatrobot.py:521
msgid "count failures as rolls higher than `n`"
msgstr ""

#: albatrobot.py:522
msgid "count successes as rolls lower than `n`"
msgstr ""

#: albatrobot.py:523
msgid "count successes as rolls higher than `n`"
msgstr ""

#: albatrobot.py:524
msgid "penetrate rolls equal to dice size"
msgstr ""

#: albatrobot.py:525
msgid "penetrate rolls lower than `n`"
msgstr ""

#: albatrobot.py:526
msgid "penetrate rolls higher than `n`"
msgstr ""

#: albatrobot.py:527
msgid "add `n` to each roll"
msgstr ""

#: albatrobot.py:528
msgid "subtract `n` from each roll"
msgstr ""

#: albatrobot.py:529
msgid "multiply by `n` each roll"
msgstr ""

#: albatrobot.py:530
msgid ""
"reroll each dice that rolled `n` until there are no `n` left\\(defaults to "
"`1`\\)"
msgstr ""

#: albatrobot.py:532
msgid "reroll once each dice that rolled `n` \\(defaults to 1\\)"
msgstr ""

#: albatrobot.py:534
msgid ""
"reroll dice that rolled lower than `n` until there are no results lower than "
"`n` left"
msgstr ""

#: albatrobot.py:536
msgid ""
"reroll dice that rolled higher than `n` until there no results higher than "
"`n` left"
msgstr ""

#: albatrobot.py:538
msgid "reroll once dice that roll lower than `n`"
msgstr ""

#: albatrobot.py:539
msgid "reroll once dice that roll higher than `n`"
msgstr ""

#: albatrobot.py:544
msgid ""
"To roll, use the syntax `\\<number of dice\\>d\\<dice size\\>`\\. You can "
"also use the following modifiers\\. Note that:\\n\\- `\\(n\\)` means that an "
"integer number may be specified, but if it isn't the specified default value "
"is assumed\n"
"\\- `n` means that an integer number is required\n"
"\\- any other symbol means exactly that symbol\n"
"\n"
"\n"
msgstr ""

#: albatrobot.py:563
msgid "You can roll up to 500 dice with up to 500 faces at once!"
msgstr ""

#: albatrobot.py:570
#, python-brace-format
msgid "{user_name} rolled {rolls} = {result}"
msgstr ""

#: albatrobot.py:579
msgid "You used a wrong syntax!"
msgstr ""
