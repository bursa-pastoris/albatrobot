---
gitea: none
include_toc: true
---

Albatrobot is a personal project born as a simple bot to roll dice, but has
since then evolved with the addition of more functions, most of which is
useless.  It thus became to the current bloat hodgepodge - but I like it the
same.


## Setup

Abatrobot is supposed to run in [Debian](https://www.debian.org/), a free
operating system.  It should work on any other operating system, but it's not
tested on any.

Albatrobot is written in [Python 3](https://www.python.org/), therefore Python
is required to use it.  Python 3 is part of any standard Debian distribution,
but you may have to install it manually in other operating systems (especially
in non-GNU/Linux based ones).

### Installation

1. Download the source code
2. Move it to the installation path of your choice and run
    ```
    python3 -m venv .venv
    source .venv/bin/activate
    python3 -m pip install -r requirements.txt
    ```

### Configuration

All the required configuration is in `settings.ini` file, that must edited
manually.  Some keys already contain a value: those are suggested values, that
can be tuned to user's needs.

- `Bot`
  - `Token`: the token to connect the bot script to the bot user on Telegram
    server.[^token]
- `Users`
  - `Users`: a comma-separated list of the user IDs of the users allowed to use
    the bot.
  - `Admins`: a comma-separated user IDs of the users with admin powers. Admins
    are always considered Users as well.
- `Settings`
  - `Language`: the language of the bot interface.  It must be specified with
    its 2 letters code.  The available languages are the ones listed in
    [`locales`](./locales).
  - `ChristmasCooldown`: the minimum time, in seconds, between two consecutive
    uses of command `/Christmas` by the same user.
  - `EasterCooldown`: same as above, for `/Easter`.
  - `Proxy`: proxy settings to connect to Telegram's bot API. The string must
    follow the syntax
    `<protocol>://[<username[:<password>]@]<hostname>[:<port>]` (e.g.
    `socks5://127.0.0.1:9050`).
- `Resources`
  - `CiteArchive`: path of the document containing the citations for `/Easter`
    command.
  - `ImgPath`: path of the document containing the images for `Christmas`
    command.

### Usage

Albatrobot can be automatically run at system boot by adding the following
command in `crontab`'s queue the following command:

```
@reboot sleep 10 && cd <installation path> && ./main.py > /tmp/albatrobot.txt 2>&1
```

To do so, run `crontab -e` and add the above line to the file that will be
opened, then save and close.

If you run Albatrobot this way - the recommended one - consider the following:

1. `sleep 10` adds a delay of 10 seconds.  It may be necessary to allow the
  system to set the network up and prevents errors due to impossibility to
  connect to the Internet and to Telegram APIs.
2. If Albatrobot can't be launched, some error information should be saved in
  `/tmp/albatrobot.txt`.

If the bot is stopped, it can be started again with the following commands:

```
cd <installation path>
source .venv/bin/activate
nohup ./main.py &
```

The bot can be stopped by killing its process on the machine it runs on or by
any admin with the `/stop` command.

For testing purposes, Albatrobot can be run with the following command:

```
cd <installation path> && source .venv/bin/activate && ./main.py
```

It can then be killed with `Ctrl + C`.


## Commands

Commands available for all users:

- `/carnival <word>`: sets a bad word, and when a user writes it in a message he gets
  berated after a delay of 5-30 seconds. The bad word is removed after
  berating, or using the command without arguments. A user can only set 1 bad
  word per chat; if one exists for the current chat, it is overwritten by the
  new one.
- `/christmas`: get an image
- `/easter (<author>)`: get a citation from given author, or a random one if no
  author is specified.
- `/end_of_year_dinner`: send all citations in the archive to a random user.
- `/help`: get help about bot commands.
- `/legal`: get legal info about privacy and copyright.
- `/roll <dice>(<modifiers>)`: roll dice, with or without modifiers. Launch
  without arguments to get help on the modifiers.
- `/version`: get Albatrobot's version

Additional commands for bot admins:

- `/init`: reinitialize the bot. This updates inline suggestions and removes
  scheduled jobs. Currently, the only jobs that may be scheduled are
  `/carnival` beratings.
- `/stop`: stop Albatrobot. To be used only in case of emergency: restarting
  the bot after this command is used requires direct access to the server.


## Common problems

### When I try to start the bot Python exits with an error

Check:

1. that the configuration file content is in the correct format and contains
   the correct information (see paragraph [Configuration](#configuration));
2. that you have installed the required libraries (see paragraph
   [Installation](#installation));
3. that you are using the virtual environment (see paragraph
   [Installation](#installation));
4. that the virtual environment has not been changed after the installation.

In the first three cases, the solution is self-explaining. In the fourth, the
simplest way is to just delete the virtual environment and recreate it. Just
run the following commands in the installation path:

```
rm -rf .venv
python3 -m venv .venv
source .venv/bin/activate
python3 -m pip install -r requirements.txt
```

This does not cause any data loss.

### The bot seems correctly booted but it doesn't work or behaves oddly

Check:

1. that you have used the correct token (see paragraph
   [Configuration](#configuration));
2. that only one instance of the albatrobot is active for the same Telegram bot
   user.

### Albatrobot says that there are no avilable citations or images

Check:

1. that there are available citations and images;
2. that citations and images are in supported formats;
3. that the correct paths are set in the configuration .

### Proposed solutions don't work and anything else

You can [open an
issue](https://git.disroot.org/bursa-pastoris/albatrobot/issues/new) with all
the necesary details on the [Disroot
repository](https://git.disroot.org/bursa-pastoris/albatrobot) or send an email
to `bursapastoris@disroot.org` taking the precautions described
[here](https://git.disroot.org/bursa-pastoris/info/src/branch/master/contact.md).

## A note about the repository

This git repository does not contain all the project's development history
because of two causes.

1. At the beginning, the project was not tracked with git.
2. I chose to redistribute the software after much time of development, so the
   original archive contained some data that I didn't want nor could publish.
   But at that time it was way easier to create a new archive than purging
   those data from the original one. Also, the original archive contained many
   completely unuseful files.

## Legal

For all legal info see [doc/LEGAL.md](doc/LEGAL.md).

* * *

![](https://www.gnu.org/graphics/agplv3-with-text-162x68.png "AGPL-3.0 logo")


[^token]: See also [How do I create a
  bot?](https://core.telegram.org/bots#3-how-do-i-create-a-bot) paragraph on
  *Bots: An introduction for developers* and [Authorizing your
  bot](https://core.telegram.org/bots/api#authorizing-your-bot) paragraph of
*Telegram Bot API* on [Telegram's API portal](https://core.telegram.org/)
